import requests
import datetime
import os

MunichCity = 900
Munich = 272

def getImageUrl(time, location=Munich):
    return "https://kachelmannwetter.com/images/data/cache/radarde/radarde_{0:%Y}_{0:%m}_{0:%d}_{location}_{0:%H}{0:%M}.png".format(time, location=location)
def getImageFilename(time, location=Munich):
    return "radarde_{0:%Y}_{0:%m}_{0:%d}_{location}_{0:%H}{0:%M}.png".format(time, location=location)

now = datetime.datetime.now()
date = datetime.datetime(now.year, now.month, now.day, now.hour, now.minute - now.minute % 5) 
step = datetime.timedelta(0, 300) # 5 min

stopDate = datetime.datetime(2017, 4, 1)

session = requests.Session()
while date > stopDate:
    #download
    imageName = getImageFilename(date)
    path = 'radar/' + imageName
    print('File: ' + imageName + '...', end='')
    if not os.path.isfile(path):
        url = getImageUrl(date)
        response = session.get(url)

        if response.status_code == 200:
            fp = open(path, 'wb')
            #perform the write operation
            fp.write(response.content)
            #close the file
            fp.close()
            print(' done')
        else:
            print(' failed Code: {}'.format(response.status_code))
    else:
        print(' exists')
        #break
    date -= step

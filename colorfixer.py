from PIL import Image
import os
import numpy
from multiprocessing import Pool

def convertImage(source, target, scale):
    try:
        img = Image.open(source)
    except:
        #delete the broken file
        if os.path.isfile('broken/'+source):
            os.remove(source)
        else:
            os.rename(source, 'broken/'+source)
        return
    img = img.convert("RGB")
    data = numpy.array(img)
    for k,v in scale.items():
        data[(data == k).all(axis = -1)] = (v, v, v)
    img = Image.fromarray(data, mode='RGB')
    img.convert('L').save(target)

def convert(filenames, scale):
    for filename in filenames:
        print('Converting: ' + filename + '...')
        convertImage("radar/"+filename, 'radarout/'+filename, scale)

def convertHelper(t):
    (filename, scale) = t
    convertImage("radar/"+filename, 'radarout/'+filename, scale)
    return filename

def convertParallel(filenames, scale):
    with Pool(6) as p:
        for f in p.imap(convertHelper, [(f, scale) for f in filenames]):
            print('Converted: ' + f + '...')

files = [f for f in os.listdir("radar") if not os.path.isfile("radarout/"+f)]


if __name__ == '__main__':
    scaleImage = Image.open('RADARDE2.png')
    scaleImage.convert('RGB')
    scale = {scaleImage.getpixel((8 + i * 12, 10))[:-1]:(i+3)*4 for i in range(61)}
    scale[(110,110,110)] = 0
    scale[(0, 246, 236)] = 4
    scale[(0, 226, 238)] = 8
    
    print(scale)
    print(len(scale))

    #convert(files)
    convertParallel(files, scale)